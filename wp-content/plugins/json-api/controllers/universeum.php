<?php
/*
 Controller name: universeum
Controller description: universeum methods
*/

class JSON_API_Universeum_Controller {

	public function get_recent_posts() {
		global $json_api;
		$posts = $json_api->introspector->get_posts();
		return $this->posts_result($posts);
	}

	protected function posts_result($posts) {
		global $wp_query;
		return array(
				'count' => count($posts),
				'count_total' => (int) $wp_query->found_posts,
				'pages' => $wp_query->max_num_pages,
				'posts' => $posts
		);
	}

		public function get_checkpoints(){
		$array = array();
		$args = array(
				'numberposts' => -1,
				'post_type' => 'checkpoint');

		$loop = new WP_Query($args);
		$counter = 0;
		while($loop -> have_posts()): $loop -> the_post();
		$array['checkpoints'][$counter]['title'] = get_the_title() ;
		$array['checkpoints'][$counter]['id'] = get_the_id() ;
		$array['checkpoints'][$counter]['thumbnail'] =  get_the_post_thumbnail() ;
		$array['checkpoints'][$counter]['beacon_selected'] = get_field('beacon_selected') ;
		$counter++;
		endwhile;
		return $array;
	}

	public function get_checkpoint(){
		global $json_api;
		$id = $json_api->query->id;
		$array = array();
		$array['result']['title'] = get_the_title($id);
	//	$array['result']['img'] = get_attachment_url(get_post_thumbnail_id($id));

		$custom = get_post_custom($id);
		foreach ( $custom as $key => $value ) {
			$array['result'][$key] = array_shift($value);
		}

		return $array;
	}
	
	public function get_beacons(){
		$array = array();
		$args = array(
				'numberposts' => -1,
				'post_type' => 'beacon');

		$loop = new WP_Query($args);
		$counter = 0;
		while($loop -> have_posts()): $loop -> the_post();
		$array['beacons'][$counter]['title'] = get_the_title() ;
		$array['beacons'][$counter]['id'] = get_the_id() ;
		$array['beacons'][$counter]['mac'] = get_field('beacon_mac') ;
		$array['beacons'][$counter]['proximity'] = get_field('proximity') ;

		$counter++;
		endwhile;
		return $array;
	}

	public function get_beacons_mac(){
		$array = array();
		$args = array(
				'numberposts' => -1,
				'post_type' => 'beacon');

		$loop = new WP_Query($args);
		$counter = 0;
		while($loop -> have_posts()): $loop -> the_post();
		$array['beacons'][$counter] = get_field('beacon_mac');
		$counter++;
		endwhile;
		return $array;
	}


	public function get_beacon(){
		global $json_api;
		$mac = "FB:47:61:15:E3:1D";
		$mac = $json_api->query->id;
		$array = array();
		$array['result']['title'] = get_the_title($mac);

		$custom = get_post_custom($id);
		foreach ( $custom as $key => $value ) {
			$array['result'][$key] = array_shift($value);
		}

		return $array;
	}

		function get_fields(){
		
		global $json_api;
		$mac = $json_api->query->mac;
		$array = array();
		$return = array();
		$args = array(
			'numberposts' => -1,
			'post_type' => 'beacon');
			
		$loop = new WP_Query($args);
		$counter = 0;
		while($loop -> have_posts()): $loop -> the_post();		
		$mac2 = get_field('mac');
		if( $mac == $mac2 ){
			$return =  get_the_id();
		}
		$counter++;
		endwhile;
		$id = $return;
		$args = array(
				'numberposts' => -1,
				'post_type' => 'checkpoint');

		$loop = new WP_Query($args);
		while($loop -> have_posts()): $loop -> the_post();
		$beaconId = get_field('beacons');
		if( $mac == $beaconId ){
			$arrayCheckpoints['checkpoints']['title'] = get_the_title();
			$arrayCheckpoints['checkpoints']['image_url'] =  get_field('image_url');
			$arrayCheckpoints['checkpoints']['description'] =  get_field('description');
			$arrayCheckpoints['checkpoints']['hasnext'] = get_field('hasnext');
		}
		endwhile;	
		return $arrayCheckpoints;
	}
		function get_zones(){
		global $json_api;
		$mac = $json_api->query->mac;
		$array = array();
		$return = array();
		$args = array(
			'numberposts' => -1,
			'post_type' => 'beacon');
			
		$loop = new WP_Query($args);
		$counter = 0;
		while($loop -> have_posts()): $loop -> the_post();		
		$mac2 = get_field('mac');
		if( $mac == $mac2 ){
			$return =  get_the_id();
		}
		$counter++;
		endwhile;
		$id = $return;

		$array = array();
		$args = array(
				'numberposts' => -1,
				'post_type' => 'zone');

		$loop = new WP_Query($args);
		while($loop -> have_posts()): $loop -> the_post();
		$beaconId = get_field('beacon');
		if (in_array($id, $beaconId)){
			$arrayCheckpoints['zone']['title'] = get_the_title();
			$arrayCheckpoints['zone']['range'] =  get_field('range');
		}
		endwhile;	
		return $arrayCheckpoints;
	}

	public function get_all_zones(){
		$array = array();
		$args = array(
				'numberposts' => -1,
				'post_type' => 'zone');

		$loop = new WP_Query($args);
		$counter = 0;
		while($loop -> have_posts()): $loop -> the_post();
		$array['zones'][$counter]['title'] = get_the_title();
		$array['zones'][$counter]['id'] = get_the_id();
		$array['zones'][$counter]['range'] =  get_field('range');
		$array['zones'][$counter]['beacon'] = get_field('beacon');

		$res = "Fail value";
		$res = 0;
			//$counter2 = 0;
		foreach ( $array['zones'][$counter][beacon] as $key => $counter2){
			$id =  $array['zones'][$counter][beacon][$key];
			$custom_fields = get_post_custom($id);
 			$my_custom_field = $custom_fields['mac'];

 			foreach ( $my_custom_field as $subkey => $value ) {
    				$array['zones'][$counter]['mac'][$key] = $value;
				
  			}
			$res = $counter2;
		}

		$counter++;
		endwhile;
		return $array;		
	}

	public function get_welcome(){
		global $json_api;
		$id = $json_api->query->id;
		$array = array();
		$array['result']['title'] = get_the_title($id);
		$array['result']['img']  = wp_get_attachment_url( get_post_thumbnail_id( $post->ID));
		$array['result']['content'] = get_post_field('post_content', $id);
		return $array;
	}
}
?>
