<?php
/*
Controller name: generic
Controller description: Mybeacon generic post and get methods
*/

class JSON_API_generic_Controller {


  public function get_all_beacons(){
    $array = array();
    $args = array(
      'numberposts' => -1,
      'post_type' => 'beacon');

      $loop = new WP_Query($args);
      $counter = 0;
      while($loop -> have_posts()): $loop -> the_post();
      $array['beacons'][$counter]['id'] = get_the_id() ;
      $array['beacons'][$counter]['title'] = get_the_title();
      $array['beacons'][$counter]['uuid'] = get_field('uuid');
      $array['beacons'][$counter]['major'] =  get_field('major');
      $array['beacons'][$counter]['minor'] = get_field('minor');
      $array['beacons'][$counter]['proximity'] = get_field('proximity');
      $counter++;
    endwhile;
    return $array;
  }

  public function get_all_messages_types(){

    $array = array();
    $args=array(
      'hide_empty' => 0
    );
    
   $terms = get_terms( 'message_type',$args);
    $counter = 0;
      foreach ( $terms as $term ) {
        $array[$counter]['term_id'] = $term->term_id;
        $array[$counter]['name'] = $term->name;
        $counter++;
      }
    return $array;
  }


  /* Debuging method */
  public function get_hotspots(){
    $array = array();
    $args = array(
      'numberposts' => -1,
      'post_type' => 'hotspot');

      $loop = new WP_Query($args);
      $counter = 0;
      while($loop -> have_posts()): $loop -> the_post();
      $array['hotspots'][$counter]['title'] = get_the_title() ;
      $array['hotspots'][$counter]['id'] = get_the_id() ;
      $id = get_the_id() ;
      $array['hotspots'][$counter]['thumbnail'] =  get_the_post_thumbnail() ;
      $array['hotspots'][$counter]['beacon'] = get_field('beacon_id') ;
      /*		$array['hotspots'][$counter]['message_type'] = get_post_meta($id, 'message_type',true);*/
      $array['hotspots'][$counter]['message_type'] = get_field('message_type');
      $array['hotspots'][$counter]['beacon_selected'] = get_field('beacon_selected') ;
      $counter++;
    endwhile;
    return $array;
  }

  /* Debuging method */
  public function get_hotspot(){
    global $json_api;
    $id = $json_api->query->id;
    $array = array();
    $array['result']['title'] = get_the_title($id);
    $array['result']['message_type'] = get_field('message_type');
    //		$array['result']['date'] = get_field('event_date',$id);

    //		$test =	get_post_custom_values('test',$id);
    $test = get_field('event_date', $id);
    //	$array['result']['date'] = $my_custom;
    $my_custom_field =	get_post_meta($id,'message_type','true');

    //		get_post_custom_values($key, $post_id);
    //	foreach ( $my_custom_field as $key => $value ) {
    //		$term = get_term_by( 'id', $value, 'message_type');
    //		$array2[] = $term->name;
    //	}
    //	$array['result']['test'] = $array2 ;
    //$array['result']['img'] = get_attachment_url(get_post_thumbnail_id($id));
    $custom = get_post_custom($id);
    foreach ( $custom as $key => $value ) {
      //$array['result'][$key] = array_shift($value);
      $array['result'][$key] = $value;
    }
    return $array;
  }

  function get_hotspot_ByBeaconId(){
    global $json_api;
    $beacon_id = $json_api->query->id;
    $arrayHotspots = array();
    $args = array(
      'numberposts' => -1,
      'post_type' => 'hotspot');

      $beaconId = 0;
      $loop = new WP_Query($args);
      while($loop -> have_posts()): $loop -> the_post();
      $beaconId = get_field('beacon_id');
      if( $beacon_id == $beaconId[0]){
        $post = get_the_id();
        $arrayHotspots['hotspot']['id'] = get_the_id();
        $arrayHotspots['hotspot']['title'] = get_the_title();
        $arrayHotspots['hotspot']['content'] = get_the_content();
        $arrayHotspots['hotspot']['img']  = wp_get_attachment_url( get_post_thumbnail_id( $postj->ID));
        $arrayHotspots['hotspot']['message_type']  = get_field('message_type');
      }
    endwhile;
    return $arrayHotspots;
  }

  function get_hotspot_ByMac(){
    global $json_api;
    $mac = $json_api->query->mac;
    $array = array();
    $return = array();
    $args = array(
    'numberposts' => -1,
    'post_type' => 'beacon');

    $loop = new WP_Query($args);
    $counter = 0;
    while($loop -> have_posts()): $loop -> the_post();
    $mac2 = get_field('beacon_mac');
    if( $mac == $mac2 ){
      $beacon_id =  get_the_id();
    }
    $counter++;
    endwhile;

    $arrayHotspots = array();
    $args = array(
    'numberposts' => -1,
    'post_type' => 'hotspot');

    $beaconId = 0;
    $loop = new WP_Query($args);
    while($loop -> have_posts()): $loop -> the_post();
    $beaconId = get_field('beacon_id');
    if( $beacon_id == $beaconId[0]){
      $post = get_the_id();
      $arrayHotspots['hotspot']['id'] = get_the_id();
      $arrayHotspots['hotspot']['title'] = get_the_title();
      $arrayHotspots['hotspot']['content'] = get_the_content();
      $arrayHotspots['hotspot']['img']  = wp_get_attachment_url( get_post_thumbnail_id( $postj->ID));
      $arrayHotspots['hotspot']['message_type']  = get_field('message_type');
    }
    endwhile;
    return $arrayHotspots;
  }

  public function get_messages_by_hotspot(){
    global $json_api;
    $id = $json_api->query->id;
    $my_custom_field =	get_post_meta($id,'message_type','true');
    foreach ( $my_custom_field as $key => $message_id ) {
      $term = get_term_by( 'id', $message_id, 'message_type');
      $array[] = $term->name;
    }
    $arrayMessages['message_type'] = $array;
    $custom = get_post_meta($id, 'msg_id',true);
    $counter = 0;
    foreach ( $custom as $key => $message_id ) {
      $arrayMessages[$counter]['id'] = $message_id;
      $arrayMessages[$counter]['title'] = get_the_title($message_id);
      $arrayMessages[$counter]['date'] = get_field('event_date',$message_id);
      $arrayMessages[$counter]['location'] = get_field('location',$message_id);
      $arrayMessages[$counter]['thumbnail'] =  wp_get_attachment_url( get_post_thumbnail_id($message_id));
      $arrayMessages[$counter]['content'] = get_post_field('post_content', $message_id);
      $array2 = 0;
      $my_custom_field =	get_post_meta($message_id,'message_type','true');
      foreach ($my_custom_field as $key2 => $value2) {
        $term = get_term_by('id', $value2, 'message_type');
        /*	$array2 = $term->name; */
        $array2 = $term->name;
      }
      $arrayMessages[$counter]['message_type'] = $array2;
      $counter++;
    }
    return $arrayMessages;
  }
}
?>
